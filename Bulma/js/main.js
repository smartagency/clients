let menuToggle = document.querySelector('.navbar-burger');
let menu = document.querySelector('.navbar-menu');

menuToggle.addEventListener('click', function() {
  this.classList.toggle('is-active');
  menu.classList.toggle('is-active');
})

// tabs

let tabButtons = document.querySelectorAll('.forms__tabs li');

let tabItems = document.querySelectorAll('.tabs__item');

for (let i = 0; i< tabButtons.length; i++) {
  tabButtons[i].addEventListener('click', function() {
    document.querySelector('.forms__tabs .is-active').classList.remove('is-active');
    this.classList.add('is-active');
    let numberItem = this.getAttribute('data-tabs');
    console.log(numberItem);
    for (let j = 0; j<tabItems.length; j++) {

      if (tabItems[j].getAttribute('data-tabs') === numberItem) {
        tabItems[j].classList.add('is-active');
        setTimeout(function(){
          tabItems[j].classList.add('animate');
        }, 10);
      } else {
        tabItems[j].classList.remove('is-active');
        tabItems[j].classList.remove('animate');
      }
    }
  })
}