$(document).ready(function () {

  $('.nav-tabs a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
      if( $(this).is(".dropdown-menu li a")){
          $("#dropdownTabName").html($(this).html());
          $("#dropDownTabsLink").dropdown("toggle");
      }
  });
});

$(document).ready(function(){
	$('.theright-header__toggler').click(function(){
		$('.h-nav-toggler').toggleClass('open');
		$('body').toggleClass('overflow')
	});
});
  
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();