 // carousels

let carouselOwners = document.querySelector("#slider-owners");

if (carouselOwners) {
    let options = {
        wrapAround: true,
        resize: true,
        adaptiveHeight: false,
        prevNextButtons: false,
        pageDots: true,
        autoPlay: 8000,
        imagesLoaded: true
    }
    
    var carouselOwnersSlide = new Flickity(carouselOwners, options);
    window.addEventListener('load', function () {
      carouselOwnersSlide.resize()
    })
}

let carouselNews = document.querySelector("#slider-news");

if (carouselNews) {
    let options = {
        wrapAround: true,
        resize: true,
        adaptiveHeight: false,
        prevNextButtons: false,
        pageDots: true,
        autoPlay: 8000,
        imagesLoaded: true
    }
    
    var carouselNewssSlide = new Flickity(carouselNews, options);
    window.addEventListener('load', function () {
      carouselNewsSlide.resize()
    })
}

let heroCarousel = document.querySelector(".hero-slider__main-carousel");

if (heroCarousel) {
    let options = {
      wrapAround: true,
      autoPlay: false,
      resize: true,
      prevNextButtons: true,
      pageDots: false
    }
    
    var heroCarouselSlide = new Flickity(heroCarousel, options);
    window.addEventListener('load', function () {
        heroCarouselSlide.resize()
    })
}


let heroNavCarousel = document.querySelector(".hero-slider__nav-carousel");

if (heroNavCarousel) {
    let options = {
      asNavFor: heroCarousel,
      wrapAround: true,
      contain: true, 
      pageDots: false, 
      prevNextButtons: true,
      setGallerySize: false,
      autoplay: false,
      draggable: false,
      resize: true
    }
    
    var heroNavCarouselSlide = new Flickity(heroNavCarousel, options);
    window.addEventListener('load', function () {
        heroNavCarouselSlide.resize()
    })
}

let galerySlider = document.querySelector(".carousel-gallery__slider");

if (galerySlider) {
    let options = {
        wrapAround: true,
        resize: true,
        adaptiveHeight: false,
        prevNextButtons: true,
        pageDots: false,
        autoPlay: false,
        imagesLoaded: true
    }
    
    var galerySliderSlide = new Flickity(galerySlider, options);
    window.addEventListener('load', function () {
      galerySliderSlide.resize()
    })
}

// filter - small demonstration

if (window.innerWidth > 991) {
    let buttonColumn = document.querySelector('#columnsAlign')
    let buttonRow = document.querySelector('#rowAlign')
    let itemCards = document.querySelectorAll('.filter__item-card')

    function filterCardsColumn(items) {
        for (let i = 0; i < items.length; i++) {
            let item = items[i];
            item.classList.remove('filter__full-card')
            if (i > 3) {
                item.style.display = 'block'
            }
        }
    }

    function filterCardsRow(items) {
        for (let i = 0; i < items.length; i++) {
            let item = items[i];
            item.classList.add('filter__full-card')
            if (i > 3) {
                item.style.display = 'none'
            }
        }
    }

    if (buttonColumn){buttonColumn.addEventListener('click', function(event) {
            buttonRow.classList.remove('active')
            buttonColumn.classList.add('active')
            filterCardsColumn(itemCards)
        })}

    if (buttonRow){buttonRow.addEventListener('click', function(event) {
            buttonColumn.classList.remove('active')
            buttonRow.classList.add('active')
            filterCardsRow(itemCards)
        })}
}


///// filterable - news

 filterSelection("all")
function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("filterDiv");
  if (c == "all") c = "";
  // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
  }
}

// Show filtered elements
function w3AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
}

// Hide elements that are not selected
function w3RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(" ");
}

// Add active class to the current control button (highlight it)
var btnContainer = document.getElementById("myBtnContainer");
var btns = btnContainer.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}