var swiperV = new Swiper(".swiper-container-v", {
  direction: "vertical",
  slidesPerView: 1,
  mousewheel: true,
  centeredSlides: false,
  slidesPerGroupSkip: 1,
  keyboard: { enabled: true },
  breakpoints: { 1024: { slidesPerView: 2, slidesPerGroup: 2 } },
  scrollbar: { el: ".swiper-scrollbar" },
  navigation: {
    nextEl: ".swiper-button-next-v",
    prevEl: ".swiper-button-prev-v",
  },
  pagination: {
    el: ".swiper-pagination-v",
    clickable: true,
  },
});

var swiperH = new Swiper(".swiper-container-h", {
  spaceBetween: 50,
  slidesPerView: 1,
  slidesPerGroupSkip: 1,
  keyboard: { enabled: true },
  effect: "fade",
  speed: 1000,
  nested: true,
  loop: true,
  navigation: {
    nextEl: ".swiper-button-next-h",
    prevEl: ".swiper-button-prev-h",
  },
  pagination: {
    el: ".swiper-pagination-h",
    clickable: true,
  },
});
