function drawStepsPlaceholder() {
  const navParent = document.querySelector(".nav-step");
  const navContainer = document.querySelector(".nav-step__container");
  const navItem = document.querySelector(".nav-step__item");
  const navPlaceholderLeft = document.querySelector(".nav-step__left-placeh");
  const navPlaceholderRight = document.querySelector(".nav-step__right-placeh");

  let marginToEndScreen =
    (navParent.clientWidth - navContainer.clientWidth) / 2 + 15;
  let navItemWidth = navItem.clientWidth;

  navPlaceholderLeft.setAttribute(
    "style",
    `width: ${navItemWidth + marginToEndScreen}px`
  );
  navPlaceholderRight.setAttribute(
    "style",
    `width: ${navItemWidth + marginToEndScreen}px`
  );
}

drawStepsPlaceholder();
window.addEventListener("resize", drawStepsPlaceholder);
