const { src, dest, series, parallel, watch } = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCss = require('gulp-clean-css');
const rename = require('gulp-rename');
const imagemin = require('gulp-imagemin');
const del = require('del');
const browserSync = require('browser-sync').create();
const pug = require('gulp-pug-3');


function pugL(){
    return src('src/components/*.pug')
        .pipe(
            pug({
                doctype: 'html',
                pretty: false
            })
        )
        .pipe(dest('docs'));
}

function css() {
    return src('src/css/main.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(cleanCss())
        .pipe(rename('main.css'))
        .pipe(dest('docs/css'))
        .pipe(browserSync.stream());
}

function images() {
    return src('src/img/**/*')
        .pipe(imagemin())
        .pipe(dest('docs/img'))
        .pipe(browserSync.stream());
}
function clean(){
    return del(['./docs/*']);
}
function dev(){
    browserSync.init({
        server: './docs'
    });
    watch('src/css/*.scss', css);
    watch('src/img/*', images);
    watch('src/components/*', pugL);
}
function build(){
    return series(clean, parallel(css, images, pugL));
}
exports.build = build();
exports.dev = series(clean, build(), dev);