(function ($) {
    const items = $(".gallery__container .gallery__col");
    const numItems = items.length;
    const perPage = 6;

    items.slice(perPage).hide();

    $('#pagination-container').pagination({
        items: numItems,
        itemsOnPage: perPage,
        prevText: "Previous page",
        nextText: "Next page",
        onPageClick: function (pageNumber) {
            const showFrom = perPage * (pageNumber - 1);
            const showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).css({
                "opacity":"0",
                "display":"block",
            }).show('slow').animate({opacity:1})
        }
    });


}(jQuery))